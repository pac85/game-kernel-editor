use std::ops::Deref;

use cgmath::{
    vec2, Deg, EuclideanSpace, Euler, Matrix3, Quaternion, Rad, Rotation, SquareMatrix, Vector2,
};
use game_kernel::ecs::EntitId;
use game_kernel::input::{Input, KeyTypes};
use game_kernel::subsystems::Subsystems;
use game_kernel::TransformComponent;
use game_kernel::{
    ecs::{AddEntity, World},
    Camera, CameraComponent, CameraSystem,
};

pub struct Flycam {
    camera_entity: EntitId,
    r: Vector2<f32>,
}

impl Flycam {
    fn setup_input(sbsts: &mut Subsystems) {
        sbsts.input().bind_action(KeyTypes::KeyBoard(17), "forward");
        sbsts
            .input()
            .bind_action(KeyTypes::KeyBoard(31), "backward");
        sbsts.input().bind_control("forward", "forward", 1.0);
        sbsts.input().bind_control("backward", "forward", -1.0);

        sbsts.input().bind_action(KeyTypes::KeyBoard(32), "left");
        sbsts.input().bind_action(KeyTypes::KeyBoard(30), "right");
        sbsts.input().bind_control("left", "side", 1.0);
        sbsts.input().bind_control("right", "side", -1.0);

        sbsts
            .input()
            .bind_action(KeyTypes::KeyBoard(42), "sprint".to_owned());

        sbsts
            .input()
            .bind_control("mouse_x", "canglex", 1.0 / 1000.0);
        sbsts
            .input()
            .bind_control("mouse_y", "cangley", 1.0 / 1000.0);
    }

    pub fn setup_flycam(sbsts: &mut Subsystems) -> Self {
        let camera_entity = AddEntity::new(sbsts.world.borrow_mut(), None)
            .unwrap()
            .with_component(TransformComponent::identity())
            .with_component(CameraComponent::new(Camera::default()))
            .entity;

        sbsts
            .systems_manager
            .get_system_mut::<CameraSystem>()
            .unwrap()
            .set_active_camera(camera_entity);

        Self::setup_input(sbsts);

        Self {
            camera_entity,
            r: vec2(0.0, 0.0),
        }
    }

    pub fn mouse_pos(&mut self, mouse_pos: Vector2<f32>) {
        self.r += mouse_pos / 1000f32;
        self.r.y = self.r.y.min(3.1415 / 2.0).max(-3.1415 / 2.0);
    }

    pub fn flycam<W: Deref<Target = World>>(
        &mut self,
        world: &W,
        delta_time: f32,
        input: &mut Input,
    ) {
        /*input.set_control_value("cangley", input.get_control_value("cangley").unwrap_or(0.0).min(3.1415 / 2.0).max(-3.1415 / 2.0));
        self.r = vec2(
            input.get_control_value("canglex").unwrap_or(0.0),
            input.get_control_value("cangley").unwrap_or(0.0),
        );*/

        let camera = world
            .get_entity_components::<&mut CameraComponent>(self.camera_entity)
            .unwrap();

        let rotation = Euler::new(Deg(0.0), Rad(self.r.x).into(), Rad(-self.r.y).into());
        let rot = Quaternion::from(rotation);

        let dir = rot.rotate_vector(cgmath::vec3(1.0, 0.0, 0.0));

        use cgmath::prelude::InnerSpace;
        let velocity = if input.is_action_down("sprint") {
            0.04
        } else {
            0.02
        };

        let ocamera = camera.camera;
        drop(camera);

        let mut c = world
            .get_entity_components::<&mut TransformComponent>(self.camera_entity)
            .unwrap();

        c.position += dir * delta_time * velocity * input.get_control_value("forward").unwrap();
        c.position += dir.cross(ocamera.up).normalize()
            * delta_time
            * velocity
            * input.get_control_value(&"side".to_owned()).unwrap();

        c.rotation = rot;
    }
}
