pub mod gizmos;
pub mod w98;

use ::game_kernel::core_systems::base::transform;
use ::game_kernel::core_systems::{base::transform::TransformComponent, light::LightComponent};
use ::game_kernel::ecs::World;
use ::game_kernel::{ecs::COMPONENT_FACTORY, subsystems::video::ui::egui::*};
use cgmath::*;
use egui::{vec2, *};
use game_kernel::core_systems::decal::DecalComponent;
use game_kernel::ecs::EntitId;
use game_kernel::{Camera, CameraComponent};

use ::game_kernel::core_systems::{
    relative_transform::RelativeTransformComponent, tag::TagComponent,
};
use game_kernel::input::{EventTypes, Input, KeyTypes, MouseKeys};
use game_kernel::physfs_rs::PhysFs;

use crate::dynamic_match_mut;
use game_kernel::core_systems::uuid::UuidComponent;

use std::ascii::AsciiExt;
use std::cell::RefCell;
use std::collections::VecDeque;
use std::ops::{Deref, DerefMut};

type GenericTabContext = (*mut World, *mut EditorUiState);
type GenericTabFn = Box<dyn FnMut(&mut Ui, &mut GenericTabContext) + Send + Sync>;

struct GenericTab {
    title: String,
    draw: GenericTabFn,
}

impl GenericTab {
    pub fn new(title: String, draw: GenericTabFn) -> Self {
        Self { title, draw }
    }

    pub fn ui(&mut self, ui: &mut egui::Ui, context: &mut GenericTabContext) {
        let margin = egui::style::Margin::same(4.0);

        egui::Frame::none().inner_margin(margin).show(ui, |ui| {
            (self.draw)(ui, context);
        });
    }
}

/*impl egui_dock::Tab for GenericTab {
    fn title(&mut self) -> WidgetText {
        self.title.clone().into()
    }

    fn ui(&mut self, ui: &mut egui::Ui) {
        let margin = egui::style::Margin::same(4.0);

        egui::Frame::none().inner_margin(margin).show(ui, |ui| {
            (self.draw)(ui);
        });
    }
}*/

struct TabViewer<'a> {
    context: &'a mut GenericTabContext,
}

impl<'a> egui_dock::TabViewer for TabViewer<'a> {
    type Tab = Box<GenericTab>;

    fn ui(&mut self, ui: &mut egui::Ui, tab: &mut Self::Tab) {
        tab.ui(ui, self.context);
    }

    fn title(&mut self, tab: &mut Self::Tab) -> egui::WidgetText {
        "a".into()
    }
}

pub struct EditorUi {
    pub state: EditorUiState,
    tree: egui_dock::Tree<Box<GenericTab>>,
    res: [u32; 2],
}

impl EditorUi {
    pub fn new(res: [u32; 2]) -> Self {
        use egui_dock::NodeIndex;

        let entities_node = Box::new(GenericTab::new(
            "entities".to_string(),
            Box::new(|ui, (world, dbg_ui)| unsafe { (&mut (**dbg_ui)).entities(ui, &mut **world) }),
        ));
        let inspector_node = Box::new(GenericTab::new(
            "inspector".to_string(),
            Box::new(|ui, (world, dbg_ui)| unsafe {
                (&mut (**dbg_ui)).inspector(ui, &mut **world)
            }),
        ));
        let scene_node = Box::new(GenericTab::new(
            "scene".to_string(),
            Box::new(move |ui, (world, dbg_ui)| unsafe {
                let size = ui.available_size();
                let image_widget = ui.add(
                    egui::Image::new((&mut (**dbg_ui)).viewport_tid, size).uv(Rect::from_min_size(
                        pos2(0.0, 0.0),
                        size / vec2(res[0] as f32, res[1] as f32),
                    )),
                );
                let rect = image_widget.rect;
                let pointer_pos = image_widget
                    .interact(Sense::click())
                    .interact_pointer_pos()
                    .map(|pos| pos - rect.min.to_vec2());
                //.filter(|_| image_widget.clicked())
                let dbg_ui = &mut (**dbg_ui);
                dbg_ui.viewport_pointer_pos =
                    image_widget.interact(Sense::drag()).interact_pointer_pos();
                dbg_ui.pos_delta = gizmos::paint_gizmos(
                    ui,
                    &dbg_ui.camera,
                    dbg_ui.selected_pos,
                    rect.min.to_vec2(),
                    [rect.size().x as u32, rect.size().y as u32],
                    dbg_ui.is_rotating(ui),
                );
                dbg_ui.viewport_size = [size.x as u32, size.y as u32];
                dbg_ui.viewport_select = dbg_ui.viewport_select.or(pointer_pos
                    .map(|pointer_pos| cgmath::vec2(pointer_pos.x as u32, pointer_pos.y as u32)));

                //paint decal boxes
                for (entity_id, _decal, transform) in
                    (**world).query::<(EntitId, &DecalComponent, &TransformComponent)>()
                {
                    gizmos::paint_box(
                        ui,
                        &dbg_ui.camera,
                        transform.get_matrix(),
                        if dbg_ui
                            .selected_entity
                            .map(|v| v == entity_id)
                            .unwrap_or(false)
                        {
                            egui::Color32::WHITE
                        } else {
                            egui::Color32::GRAY
                        },
                        rect.min.to_vec2(),
                        [rect.size().x as u32, rect.size().y as u32],
                    );
                    gizmos::paint_3d_arrow(
                        ui,
                        &dbg_ui.camera,
                        cgmath::Vector3::unit_z(),
                        transform.get_matrix(),
                        egui::Color32::WHITE,
                        rect.min.to_vec2(),
                        [rect.size().x as u32, rect.size().y as u32],
                    );
                }
            }),
        ));

        let mut tree = egui_dock::Tree::new(vec![entities_node]);
        let [l, r] = tree.split_left(NodeIndex::root(), 0.5, vec![scene_node]);
        tree.split_below(l, 0.5, vec![inspector_node]);

        Self {
            state: EditorUiState::new(),
            tree,
            res,
        }
    }

    pub fn draw_tabs(&mut self, ctx: &egui::Context, world: &mut World) {
        let id = egui::Id::new("some hashable string");
        let layer_id = egui::LayerId::background();
        let max_rect = ctx.available_rect();
        let clip_rect = ctx.available_rect();

        let style = egui_dock::Style::from_egui(ctx.style().as_ref());
        let mut ui = egui::Ui::new(ctx.clone(), layer_id, id, max_rect, clip_rect);
        egui_dock::DockArea::new(&mut self.tree)
            .style(style)
            .show_inside(
                &mut ui,
                &mut TabViewer {
                    context: &mut (world as *mut _, (&mut self.state) as *mut _),
                },
            );
    }
}

pub struct EditorUiState {
    pub selected_entity: Option<u64>,
    entity_filter: String,
    frametimes: [(f32, f32); 100],
    frametimes_i: usize,
    pub camera: Camera,
    selected_pos: Vector3<f32>,
    pub pos_delta: Option<Vector3<f32>>,
    pub viewport_pointer_pos: Option<egui::Pos2>,
    pub viewport_select: Option<Vector2<u32>>,
    pub viewport_tid: TextureId,
    pub viewport_size: [u32; 2],
}

impl EditorUiState {
    pub fn new() -> Self {
        Self {
            selected_entity: None,
            entity_filter: "".to_owned(),
            frametimes: [(0.0, 0.0); 100],
            frametimes_i: 0,
            camera: Camera::new(
                cgmath::Point3::new(0.0, 0.0, 0.0),
                cgmath::vec3(0.0, 1.0, 0.0),
                cgmath::vec3(0.0, 1.0, 0.0),
                cgmath::Rad(1.0),
                (0.1, 1000.0),
            ),
            selected_pos: cgmath::Vector3::zero(),
            pos_delta: None,
            viewport_pointer_pos: None,
            viewport_select: None,
            viewport_tid: Default::default(),
            viewport_size: [100, 100],
        }
    }

    fn vec3_ui(v: &mut Vector3<f32>, ui: &mut Ui, label: &str) {
        ui.horizontal(|ui| {
            ui.label(label);
            DragValue::new(&mut v.x).ui(ui);
            DragValue::new(&mut v.y).ui(ui);
            DragValue::new(&mut v.z).ui(ui);
        });
    }

    fn rot_ui(r: &mut Quaternion<f32>, ui: &mut Ui, label: &str) {
        let rotation = Euler::from(*r);
        let mut rotation: Vector3<f32> = Vector3::new(
            Deg::from(rotation.x).0,
            Deg::from(rotation.y).0,
            Deg::from(rotation.z).0,
        );
        ui.horizontal(|ui| {
            ui.label(label);
            DragValue::new(&mut rotation.x).ui(ui);
            DragValue::new(&mut rotation.y).ui(ui);
            DragValue::new(&mut rotation.z).ui(ui);
        });
        let rotation = Euler::new(Deg(rotation.x), Deg(rotation.y), Deg(rotation.z));
        *r = Quaternion::from(rotation);
    }

    pub fn frames_ui(&mut self, gk_egui: &mut GkEgui, delta: f32, active: f32, desired: &mut f32) {
        let ctx = gk_egui.context();

        self.frametimes[self.frametimes_i] = (delta, active);
        self.frametimes_i += 1;
        self.frametimes_i %= self.frametimes.len();

        //w98 tests
        w98::window(&ctx, "w98 wt", |ui| {
            for i in 0..6 {
                ui.horizontal(|ui| {
                    for i in 0..6 {
                        w98::button(ui, RichText::new("Hi").color(Color32::BLACK));
                    }
                });
            }
        });
        //end
        egui::Window::new("fps limiter").show(&ctx, |ui| {
            ui.horizontal(|ui| {
                ui.label("delta:");
                ui.label(delta.to_string());
                ui.label((1000.0 / delta.max(0.0001)).to_string());
            });
            ui.horizontal(|ui| {
                ui.label("active:");
                ui.label(active.to_string());
                ui.label((1000.0 / active.max(0.0001)).to_string());
            });
            ui.add(egui::Slider::new(desired, 30.0..=200.0).text("Desired"));

            //plot
            use egui::plot::{Line, PlotPoints};
            /*let delta_line = Line::new(PlotPoints::from_explicit_callback(|i| {
                    let shifted = (i as usize + self.frametimes_i) % self.frametimes.len();
                    self.frametimes[shifted].0 as f64
                },
                0.0..self.frametimes.len() as f64,
                self.frametimes.len()
            ));
            let active_line = Line::new(PlotPoints::from_explicit_callback(|i| {
                    let shifted = (i as usize + self.frametimes_i) % self.frametimes.len();
                    self.frametimes[shifted].1 as f64
                },
                0.0..self.frametimes.len() as f64,
                self.frametimes.len()
            ))
            .color(Color32::GREEN);*/
            egui::plot::Plot::new("example_plot")
                .height(200.0)
                .data_aspect(1.0)
                .show(ui, |plot_ui| {
                    /*plot_ui.line(delta_line);
                    plot_ui.line(active_line);*/
                });
        });
    }

    fn input_button(
        &self,
        ui: &mut Ui,
        input: &mut VecDeque<EventTypes>,
        name: &'static str,
        key: KeyTypes,
    ) {
        let res = ui.button(name);
        if res.is_pointer_button_down_on() {
            input.push_back(game_kernel::input::EventTypes::KeyDown(key));
        } else {
            input.push_back(game_kernel::input::EventTypes::KeyUp(key));
        }
    }

    pub fn touch_input_ui(
        &mut self,
        gk_egui: &mut GkEgui,
        input: &mut VecDeque<EventTypes>,
    ) -> (bool, cgmath::Vector2<f32>) {
        let ctx = gk_egui.context();
        let mut mdelta = vec2(0.0, 0.0);

        let mut toggle_captured = false;
        egui::Window::new("touch_controls").show(&ctx, |ui| {
            toggle_captured = ui.button("esc").clicked();

            ui.horizontal(|ui| {
                self.input_button(ui, input, "1", KeyTypes::KeyBoard(2));
                self.input_button(ui, input, "2", KeyTypes::KeyBoard(3));
                self.input_button(ui, input, "3", KeyTypes::KeyBoard(4));
                self.input_button(ui, input, "4", KeyTypes::KeyBoard(5));
                self.input_button(ui, input, "shoot", KeyTypes::Mouse(MouseKeys::Left));
            });
            ui.horizontal(|ui| {
                ui.button("q");
                self.input_button(ui, input, "w", KeyTypes::KeyBoard(17));
                self.input_button(ui, input, "e", KeyTypes::KeyBoard(18));
                self.input_button(ui, input, "jump", KeyTypes::KeyBoard(57));
            });
            ui.horizontal(|ui| {
                self.input_button(ui, input, "a", KeyTypes::KeyBoard(30));
                self.input_button(ui, input, "s", KeyTypes::KeyBoard(31));
                self.input_button(ui, input, "d", KeyTypes::KeyBoard(32));
            });
        });

        egui::Window::new("touch_view").show(&ctx, |ui| {
            let response = ui.allocate_response(egui::vec2(200.0, 200.0), Sense::click_and_drag());
            if response.is_pointer_button_down_on() {
                mdelta = ui.input().pointer.delta();
            }
        });

        (toggle_captured, cgmath::vec2(mdelta.x, mdelta.y))
    }

    fn is_rotating(&self, ui: &Ui) -> bool {
        ui.input().key_down(egui::Key::R)
    }

    fn inspector(&mut self, ui: &mut Ui, mut world: &mut World) {
        ui.separator();
        if self.selected_entity.is_some() && ui.button("🗑").clicked() {
            world
                .deref_mut()
                .rem_entity_recursive(self.selected_entity.unwrap());
            return;
        }
        ui.separator();

        if let Some(selected_entity) = self.selected_entity {
            if let Some(ref mut relative_transform) =
                world.get_entity_components::<&mut RelativeTransformComponent>(selected_entity)
            {
                if let Some(d) = self.pos_delta {
                    if self.is_rotating(ui) {
                        relative_transform.rotation = relative_transform.rotation
                            * Quaternion::from(Euler::new(
                                Deg(10.0 * d.x),
                                Deg(10.0 * d.y),
                                Deg(10.0 * d.z),
                            ));
                    } else {
                        relative_transform.position += d;
                    }
                }
            }
            if let Some(ref mut transform) =
                world.get_entity_components::<&mut TransformComponent>(selected_entity)
            {
                if let Some(d) = self.pos_delta {
                    if self.is_rotating(ui) {
                        transform.rotation = Quaternion::from(Euler::new(
                            Deg(10.0 * d.x),
                            Deg(10.0 * d.y),
                            Deg(10.0 * d.z),
                        )) * transform.rotation;
                    } else {
                        transform.position += d;
                    }
                }
                self.selected_pos = transform.position;
            }
            //Show children as buttons
            Label::new(RichText::new("Children").heading()).ui(ui);
            egui::ScrollArea::vertical()
                .max_height(200.0)
                .show(ui, |ui| {
                    if let Some(children) = world.get_children(selected_entity) {
                        children.for_each(|child| {
                            let mut name = format!("{}", child);
                            if let Some(tag) = world.get_entity_components::<&TagComponent>(child) {
                                name = tag.get_name().to_owned();
                            }
                            if ui.selectable_label(false, name).clicked() {
                                self.selected_entity = Some(child);
                            }
                        })
                    }
                });
            ui.separator();
            egui::ScrollArea::vertical()
                .max_height(400.0)
                .id_source("inspector_scroll_area")
                .show(ui, |ui| {
                    ui.vertical(|ui| {
                        let cf = COMPONENT_FACTORY.lock().unwrap();
                        for mut component in
                            world.deref().get_all_entity_components_mut(selected_entity)
                        {
                            Label::new(RichText::new(component.get_name_dyn()).heading())
                                .ui(ui);

                            ui.separator();
                            dynamic_match_mut!(component.gk_as_any_mut(),
                                /*(RelativeTransformComponent)(c) => {
                                    Self::vec3_ui(&mut c.position, ui, "translation:");
                                    Self::rot_ui(&mut c.rotation, ui, "rotation:");
                                    Self::vec3_ui(&mut c.scale, ui, "scale:");
                                },
                                (TagComponent)(c) => {
                                    ui.label(format!("name: {}", c.get_name()));
                                },
                                (TransformComponent)(c) => {
                                    Self::vec3_ui(&mut c.position, ui, "translation:");
                                    Self::rot_ui(&mut c.rotation, ui, "rotation:");
                                    Self::vec3_ui(&mut c.scale, ui, "scale:");
                                },*/
                                (LightComponent)(c) => {
                                    let mut color = c.light.get_color();
                                    Self::vec3_ui(&mut color, ui, "color:");
                                    c.light.set_color(color);
                                },
                                (UuidComponent)(c) => { ui.label(format!("uuid: {}", c.uuid())); },
                                _ => {
                                    for field in component.get_public_fields() {
                                        dynamic_match_mut!(component.get_field_any_mut(field).unwrap(),
                                            (Vector3<f32>)(f) => { Self::vec3_ui(f, ui, field); },
                                            (Quaternion<f32>)(f) => { Self::rot_ui(f, ui, field); },
                                            (String)(f) => { ui.label(format!("{}: {}", field, f.as_str())); },
                                            (u64)(f) => { ui.label(format!("{}: {}", field, f)); },
                                            _ => { ui.label(*field); }
                                        );
                                    }
                                }
                            );
                            ui.separator();
                        }
                    });
                });
        }
    }
    fn entities(&mut self, ui: &mut Ui, mut world: &mut World) {
        //egui::Window::new("Entities").show(&ctx, |ui| {
        ui.horizontal(|ui| {
            ui.text_edit_singleline(&mut self.entity_filter);
            if ui.button("X").clicked() {
                self.entity_filter = "".to_owned();
            }
        });
        egui::ScrollArea::vertical()
            .id_source("entities_scroll")
            .max_height(300.0)
            .auto_shrink([false; 2])
            .show(ui, |ui| {
                ui.vertical(|ui| {
                    for entity in world.iter_entities() {
                        let mut name = "".to_owned();
                        if let Some(tag) = world.get_entity_components::<&TagComponent>(entity) {
                            name = tag.get_name().to_owned();
                        }

                        if !name
                            .to_ascii_lowercase()
                            .contains(&self.entity_filter.to_ascii_lowercase())
                        {
                            continue;
                        }

                        if ui
                            .selectable_label(
                                self.selected_entity.map(|s| s == entity).unwrap_or(false),
                                format!("{} {}", entity, name),
                            )
                            .clicked()
                        {
                            self.selected_entity = Some(entity);
                        }
                    }
                });
            });
    }

    pub fn dbg_ui<W: DerefMut<Target = World>>(
        &mut self,
        gk_egui: &mut GkEgui,
        mut world: W,
        should_run: &mut bool,
    ) -> W {
        let ctx = gk_egui.context();

        egui::TopBottomPanel::top("top bar").show(&ctx, |ui| {
            egui::menu::bar(ui, |ui| {
                egui::menu::menu_button(ui, "file", |ui| {
                    *should_run = !ui.button("exit").clicked();
                });
                ui.separator();
                ui.add_space(30.0);
                ui.label("hi")
            });
        });

        /*egui::SidePanel::right("Entities").show(&ctx, |ui| {
        });*/

        world
    }

    pub fn file_tree(&self, gk_egui: &mut GkEgui, fs: &PhysFs) {
        egui::Window::new("files")
            .hscroll(true)
            .vscroll(true)
            .show(&gk_egui.context(), |ui| {
                ui.set_min_width(300.0);
                self.file_tree_inner(fs, ui, "".to_owned());
            });
    }

    fn file_tree_inner(&self, fs: &PhysFs, ui: &mut egui::Ui, dir: String) {
        let mut files = vec![];
        for file in fs.enumerate_files(&dir).unwrap().iter() {
            let cdir = dir.clone() + "/" + file;
            if fs.is_directory(&cdir) {
                egui::CollapsingHeader::new(format!("🗀{file}")).show(ui, |ui1| {
                    self.file_tree_inner(fs, ui1, cdir);
                });
            } else {
                files.push(format!("🗋{file}"));
            }
        }

        for file in files.into_iter() {
            ui.label(file);
        }
    }
}

pub struct OnSreenConsole {
    lines: VecDeque<String>,
    timeout: f32,
}

impl OnSreenConsole {
    pub fn new() -> Self {
        Self {
            lines: VecDeque::new(),
            timeout: 0.0,
        }
    }

    pub fn draw(&mut self, context: &Context, delta: f32) {
        self.timeout -= delta;
        self.timeout = self.timeout.max(0.0);

        if self.timeout == 0.0 {
            self.lines.clear();
        }

        egui::Area::new("log_area")
            .fixed_pos(egui::pos2(30.0, 30.0))
            .show(context, |ui| {
                ui.vertical(|ui| {
                    for line in self.lines.iter() {
                        ui.label(RichText::new(line).size(12.0).color(egui::Color32::WHITE));
                    }
                })
            });
    }

    pub fn print(&mut self, s: impl AsRef<str>) {
        self.lines.push_back(s.as_ref().to_owned());
        if self.lines.len() > 20 {
            self.lines.pop_front();
        }
        self.timeout = 10.0;
    }
}

pub static mut SCREEN_CONSOLE: Option<std::sync::Mutex<OnSreenConsole>> = None;

pub fn screen_print_s(s: impl AsRef<str>) {
    unsafe {
        SCREEN_CONSOLE.as_ref().unwrap().lock().unwrap().print(s);
    }
}

#[macro_export]
macro_rules! screen_print {
    ($($arg:tt)*) => { crate::dbg_ui::screen_print_s(format!($($arg)*)) }
}

pub fn init_screen_console() {
    unsafe {
        SCREEN_CONSOLE = Some(std::sync::Mutex::new(OnSreenConsole::new()));
    }
}
