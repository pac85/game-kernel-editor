use egui::{vec2, *};

fn make_rect(x: f32, y: f32, w: f32, h: f32) -> Rect {
    Rect::from_min_size(pos2(x, y), vec2(w, h))
}

fn unmake_rect(rect: Rect) -> (f32, f32, f32, f32) {
    let (x, y) = (rect.min.x, rect.min.y);
    let (w, h) = (rect.size().x, rect.size().y);

    (x, y, h, w)
}

pub fn draw_raised_rectangle(ui: &mut Ui, rect: Rect) {
    let (x, y, h, w) = unmake_rect(rect);
    let (main_color, shadow_color, high_color) = (
        egui::Color32::from_rgb(194, 198, 202),
        egui::Color32::from_rgb(133, 137, 141),
        egui::Color32::from_gray(255),
    );
    ui.painter()
        .rect_filled(make_rect(x, y, w - 1.0, h - 1.0), 0.0, main_color);

    ui.painter()
        .rect_filled(make_rect(x, y + h - 1.0, w, 1.0), 0.0, shadow_color);
    ui.painter()
        .rect_filled(make_rect(x + w - 1.0, y, 1.0, h), 0.0, shadow_color);

    ui.painter()
        .rect_filled(make_rect(x, y, w, 1.0), 0.0, high_color);
    ui.painter()
        .rect_filled(make_rect(x, y, 1.0, h), 0.0, high_color);
}

pub fn draw_sunken_rectangle(ui: &mut Ui, rect: Rect) {
    let (x, y, h, w) = unmake_rect(rect);
    let (main_color, shadow_color, high_color) = (
        egui::Color32::from_rgb(194, 198, 202),
        egui::Color32::from_rgb(133, 137, 141),
        egui::Color32::from_gray(255),
    );
    ui.painter()
        .rect_filled(make_rect(x, y, w - 1.0, h - 1.0), 0.0, main_color);

    ui.painter()
        .rect_filled(make_rect(x, y + h - 1.0, w, 1.0), 0.0, high_color);
    ui.painter()
        .rect_filled(make_rect(x + w - 1.0, y, 1.0, h), 0.0, high_color);

    ui.painter()
        .rect_filled(make_rect(x, y, w, 1.0), 0.0, shadow_color);
    ui.painter()
        .rect_filled(make_rect(x, y, 1.0, h), 0.0, shadow_color);
}

pub fn draw_double_raised_rectangle(ui: &mut Ui, rect: Rect, raised: bool) {
    let (x, y, h, w) = unmake_rect(rect);
    let (main_color, mut base_color, mut shadow_color, mut border_color, mut high_color) = (
        egui::Color32::from_rgb(194, 198, 202),
        egui::Color32::from_rgb(184, 188, 202),
        egui::Color32::from_rgb(133, 137, 141),
        egui::Color32::from_gray(150),
        egui::Color32::from_gray(255),
    );

    if !raised {
        (high_color, border_color) = (border_color, high_color);
        (border_color, base_color) = (base_color, border_color);
    }

    ui.painter()
        .rect_filled(make_rect(x, y, w - 2.0, h - 2.0), 0.0, main_color);

    ui.painter()
        .rect_filled(make_rect(x, y + h - 1.0, w, 1.0), 0.0, border_color);
    ui.painter()
        .rect_filled(make_rect(x + w - 1.0, y, 1.0, h), 0.0, border_color);

    ui.painter()
        .rect_filled(make_rect(x, y, w - 1.0, 1.0), 0.0, high_color);
    ui.painter()
        .rect_filled(make_rect(x, y, 1.0, h - 1.0), 0.0, high_color);

    ui.painter()
        .rect_filled(make_rect(x + 1.0, y + 1.0, w - 2.0, 1.0), 0.0, base_color);
    ui.painter()
        .rect_filled(make_rect(x + 1.0, y + 1.0, 1.0, h - 2.0), 0.0, base_color);

    ui.painter().rect_filled(
        make_rect(x + 1.0, y + h - 2.0, w - 2.0, 1.0),
        0.0,
        shadow_color,
    );
    ui.painter().rect_filled(
        make_rect(x + w - 2.0, y + 1.0, 1.0, h - 2.0),
        0.0,
        shadow_color,
    );
}

pub fn draw_border_rectangle(ui: &mut Ui, rect: Rect) {
    let (x, y, h, w) = unmake_rect(rect);
    let color = Color32::from_gray(0);

    ui.painter()
        .rect_filled(make_rect(x, y + h - 1.0, w, 1.0), 0.0, color);
    ui.painter()
        .rect_filled(make_rect(x + w - 1.0, y, 1.0, h), 0.0, color);

    ui.painter()
        .rect_filled(make_rect(x, y, w, 1.0), 0.0, color);
    ui.painter()
        .rect_filled(make_rect(x, y, 1.0, h), 0.0, color);
}

pub fn button_inner<R>(
    ui: &mut Ui,
    padding: Vec2,
    inner_size: Vec2,
    add_contents: impl FnOnce(&mut Ui, Pos2, &Response) -> R,
) -> Response {
    let desired_size = inner_size + padding;

    let (rect, response) = ui.allocate_exact_size(desired_size, Sense::click());

    if ui.is_rect_visible(rect) {
        draw_border_rectangle(ui, rect);
        let rect = if response.is_pointer_button_down_on() {
            rect.translate(vec2(1.0, 1.0))
        } else {
            rect
        };

        let (x, y, h, w) = unmake_rect(rect);
        if response.is_pointer_button_down_on() {
            draw_sunken_rectangle(ui, make_rect(x, y, w - 1.0, h - 1.0));
        } else {
            draw_raised_rectangle(ui, make_rect(x, y, w - 1.0, h - 1.0));
        }

        let text_pos = pos2(
            rect.min.x + padding.x / 2.0,
            rect.center().y - 0.5 * inner_size.y,
        );
        //text.paint_with_visuals(ui.painter(), text_pos, ui.style().interact(&response));
        add_contents(ui, text_pos, &response);
    }

    response
}

pub fn button(ui: &mut Ui, label: impl Into<WidgetText>) -> Response {
    let text = label
        .into()
        .into_galley(ui, None, ui.available_width(), TextStyle::Button);
    let inner_size = text.size();
    button_inner(
        ui,
        2.0 * ui.spacing().button_padding,
        inner_size,
        |ui, pos, response| {
            text.paint_with_visuals(ui.painter(), pos, ui.style().interact(response))
        },
    )
}

fn draw_triangle(ui: &mut Ui, pos: Pos2, size: Vec2) {
    let rect = Rect::from_min_size(pos, size);
    let points = vec![rect.left_top(), rect.right_top(), rect.center_bottom()];
    ui.painter().add(Shape::convex_polygon(
        points,
        Color32::BLACK,
        Stroke::new(1.0, Color32::BLACK),
    ));
}

pub fn button_triangle(ui: &mut Ui, size: f32) -> Response {
    let size2 = vec2(size + 1.0, size);
    button_inner(ui, vec2(8.0, 7.0), size2, |ui, pos, response| {
        draw_triangle(ui, pos, size2);
    })
}

pub fn title_bar(
    ui: &mut Ui,
    label: impl Into<WidgetText>,
    width: f32,
    pad: f32,
    sense: Sense,
    collapsed: &mut bool,
) -> Response {
    let (active_color) = Color32::from_rgb(0, 0, 128);
    let height = 20.0;

    let size = vec2(width - 2.0 * pad /*ui.available_size().x*/, height);
    let (rect, response) = ui.allocate_exact_size(size, sense);
    let rect = rect.translate(vec2(pad, pad));
    ui.painter()
        .rect_filled(rect, 0.0, Color32::from_rgb(194, 198, 202));
    ui.painter()
        .rect_filled(rect.shrink(1.0), 0.0, active_color);

    let mut child_ui = ui.child_ui(rect.shrink(1.0).translate(vec2(1.0, 0.0)), *ui.layout());
    child_ui.horizontal(|child_ui| {
        //button_inner(child_ui, RichText::new("\u{1F53C}").color(Color32::BLACK), false);
        *collapsed ^= button_triangle(child_ui, 7.0).clicked();
        child_ui.label(RichText::new(label.into().text()).color(Color32::WHITE));
    });

    response
}

#[derive(Clone)]
struct WindowState {
    child_rect: Rect,
    drag_offset: Option<Vec2>,
    collapsed: bool,
}

pub fn window<R: Default>(
    ctx: &Context,
    label: impl Into<WidgetText>,
    add_contents: impl FnOnce(&mut Ui) -> R,
) {
    let label = label.into();
    let mut area = Area::new(label.text());
    /*let resize_id = Id::new(label.text()).with("resize");
    let resize = Resize::default().id(resize_id);*/
    let rect = ctx
        .memory()
        .data
        .get_temp::<WindowState>(Id::new(label.text()));
    if let Some(WindowState {
        child_rect,
        drag_offset,
        collapsed,
    }) = rect
    {
        area = area.fixed_pos(child_rect.min);
    };

    area.movable(false)
        .interactable(true)
        .enabled(true)
        .show(ctx, |ui| {
            let mut width = 300.0;
            let (last_child_rect, mut collapsed) = if let Some(WindowState {
                child_rect: last_child_rect,
                drag_offset,
                collapsed,
            }) = rect
            {
                width = last_child_rect.size().x;
                draw_double_raised_rectangle(
                    ui,
                    Rect::from_min_max(last_child_rect.min, last_child_rect.max + vec2(0.0, 20.0)),
                    true,
                );

                (last_child_rect, collapsed)
            } else {
                (Rect::from_min_size(pos2(0.0, 0.0), vec2(96.0, 32.0)), false)
            };
            let title_response =
                title_bar(ui, label.clone(), width, 2.0, Sense::drag(), &mut collapsed);
            let child_rect = ui.available_rect_before_wrap();
            let mut child_ui = ui.child_ui(child_rect.shrink(5.0), *ui.layout());
            let ret = if !collapsed {
                add_contents(&mut child_ui)
            } else {
                Default::default()
            };
            let mut child_rect = child_ui.min_rect().expand(5.0).union(Rect::from_min_size(
                child_ui.min_rect().min,
                vec2(96.0, 32.0),
            ));
            if collapsed {
                child_rect.set_height(3.0);
            }
            let movement = if title_response.dragged() {
                ui.input().pointer.delta()
            } else {
                vec2(0.0, 0.0)
            };
            child_rect = Rect::from_min_size(last_child_rect.min + movement, child_rect.size());

            ctx.memory().data.insert_temp(
                Id::new(label.text()),
                WindowState {
                    child_rect,
                    drag_offset: None,
                    collapsed,
                },
            );
            let response = ui.allocate_rect(
                Rect::from_min_max(child_rect.min, child_rect.max + vec2(0.0, 20.0)),
                Sense::hover().union(Sense::click()),
            );
            InnerResponse::new(ret, response)
        });
}
