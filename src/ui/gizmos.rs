use cgmath::{prelude::*, vec3, vec4, Matrix4, Vector3, Vector4};
use egui::{Stroke, Ui};
use game_kernel::Camera;

pub fn camera_transform(camera: &Camera, pos: Vector3<f32>, res: [u32; 2]) -> Vector4<f32> {
    let camera_matrix = camera.get_proj(res[0] as f32 / res[1] as f32) * camera.get_view();
    let pos = camera_matrix * pos.extend(1.0);
    if pos.w < 0.01 {
        return Vector4::zero();
    }
    let mut pos = pos / pos.w;

    pos += vec4(1.0, 1.0, 0.0, 0.0);
    pos.mul_element_wise(vec4(0.5 * (res[0] as f32), 0.5 * (res[1] as f32), 1.0, 1.0))
}

fn line_point_distance(line: [Vector3<f32>; 2], point: Vector3<f32>) -> f32 {
    (line[0] - point).cross(line[1] - point).magnitude() / (line[0] - line[1]).magnitude()
}

fn line_point_distance2(line: [egui::Vec2; 2], point: egui::Vec2) -> f32 {
    line_point_distance(
        [
            vec3(line[0].x, line[0].y, 0.0),
            vec3(line[1].x, line[1].y, 0.0),
        ],
        vec3(point.x, point.y, 0.0),
    )
}

pub fn paint_3d_line(
    ui: &mut Ui,
    camera: &Camera,
    a: cgmath::Vector3<f32>,
    b: Vector3<f32>,
    color: egui::Color32,
    offset: egui::Vec2,
    res: [u32; 2],
) {
    ui.painter().line_segment(
        [
            camera_transform(camera, a, res),
            camera_transform(camera, b, res),
        ]
        .map(|v| egui::pos2(v.x, v.y) + offset),
        egui::Stroke::new(1.0, color),
    );
}

pub fn paint_3d_arrow(
    ui: &mut Ui,
    camera: &Camera,
    d: cgmath::Vector3<f32>,
    m: Matrix4<f32>,
    color: egui::Color32,
    offset: egui::Vec2,
    res: [u32; 2],
) {
    let trasnform_v3 = |v: Vector3<f32>, m: Matrix4<f32>| (m * v.extend(1.0)).truncate();
    let [a, b] = [trasnform_v3(cgmath::Vector3::zero(), m), trasnform_v3(d, m)];
    let [ta, tb] = [
        camera_transform(camera, a, res),
        camera_transform(camera, b, res),
    ]
    .map(|v| egui::pos2(v.x, v.y) + offset);
    ui.painter().arrow(
        ta,
        tb.to_vec2() - ta.to_vec2(),
        egui::Stroke::new(1.0, color),
    );
}

pub fn paint_box(
    ui: &mut Ui,
    camera: &Camera,
    m: Matrix4<f32>,
    color: egui::Color32,
    offset: egui::Vec2,
    res: [u32; 2],
) {
    let trasnform_v3 = |v: Vector3<f32>, m: Matrix4<f32>| (m * v.extend(1.0)).truncate();
    let mut paint_edge = |a, b| {
        paint_3d_line(
            ui,
            camera,
            trasnform_v3(a, m),
            trasnform_v3(b, m),
            color,
            offset,
            res,
        )
    };
    let points = [
        cgmath::vec3(1.0, 1.0, -1.0),
        cgmath::vec3(1.0, -1.0, -1.0),
        cgmath::vec3(1.0, 1.0, 1.0),
        cgmath::vec3(1.0, -1.0, 1.0),
        cgmath::vec3(-1.0, 1.0, -1.0),
        cgmath::vec3(-1.0, -1.0, -1.0),
        cgmath::vec3(-1.0, 1.0, 1.0),
        cgmath::vec3(-1.0, -1.0, 1.0),
    ];
    paint_edge(points[0], points[1]);
    paint_edge(points[0], points[2]);
    paint_edge(points[2], points[3]);
    paint_edge(points[4], points[0]);
    paint_edge(points[1], points[5]);
    paint_edge(points[2], points[6]);
    paint_edge(points[5], points[7]);
    paint_edge(points[7], points[6]);
    paint_edge(points[3], points[1]);
    paint_edge(points[4], points[5]);
    paint_edge(points[7], points[3]);
    paint_edge(points[6], points[4]);
}

#[derive(Debug, Clone, Copy)]
pub enum Axis {
    X,
    Y,
    Z,
}

impl Axis {
    pub fn from_i(i: u32) -> Self {
        match i {
            0 => Self::X,
            1 => Self::Y,
            2 => Self::Z,
            _ => Self::X,
        }
    }

    pub fn versor(&self) -> Vector3<f32> {
        match self {
            Self::X => vec3(1.0, 0.0, 0.0),
            Self::Y => vec3(0.0, 1.0, 0.0),
            Self::Z => vec3(0.0, 0.0, 1.0),
        }
    }

    pub fn rotated(&self) -> Self {
        match self {
            Self::X => Self::Z,
            Self::Y => Self::X,
            Self::Z => Self::Y,
        }
    }
}

pub fn paint_elliposoid(
    ui: &mut Ui,
    center: egui::Pos2,
    r: egui::Vec2,
    nr: egui::Vec2,
    color: egui::Color32,
) {
    let r = r * 2.0;
    let nr = nr * 2.0;
    let shape = egui::epaint::CubicBezierShape::from_points_stroke(
        [
            center + egui::vec2(0.0, -nr.y / 2.0),
            center + egui::vec2(r.x * 2.0 / 3.0, -nr.y / 2.0),
            center + egui::vec2(r.x * 2.0 / 3.0, r.y / 2.0),
            center + egui::vec2(0.0, r.y / 2.0),
        ],
        false,
        egui::Color32::TRANSPARENT,
        egui::Stroke::new(1.0, color),
    );
    ui.painter().add(shape);
    let shape = egui::epaint::CubicBezierShape::from_points_stroke(
        [
            center + egui::vec2(0.0, r.y / 2.0),
            center + egui::vec2(-nr.x * 2.0 / 3.0, r.y / 2.0),
            center + egui::vec2(-nr.x * 2.0 / 3.0, -nr.y / 2.0),
            center + egui::vec2(0.0, -nr.y / 2.0),
        ],
        false,
        egui::Color32::TRANSPARENT,
        egui::Stroke::new(1.0, color),
    );
    ui.painter().add(shape);
}

pub fn draw_circle(
    ui: &mut Ui,
    camera: &Camera,
    pos: Vector3<f32>,
    offset: egui::Vec2,
    res: [u32; 2],
    axis: Axis,
    color: egui::Color32,
) {
    println!("acis {axis:?}, {:?}", axis.versor());
    let circle_size = 0.35;
    let circle_dist = 0.7;
    let axes = [
        axis.versor() * circle_dist,
        axis.rotated().versor() * circle_size + axis.versor() * circle_dist,
        axis.rotated().rotated().versor() * circle_size + axis.versor() * circle_dist,
        -axis.rotated().versor() * circle_size + axis.versor() * circle_dist,
        -axis.rotated().rotated().versor() * circle_size + axis.versor() * circle_dist,
    ];

    let axes = axes.map(|v| camera_transform(camera, pos + v, res));
    let ellipsoid_center = axes[0];
    let ellipsoid_rx = (ellipsoid_center.x - axes[1].x)
        .abs()
        .max((ellipsoid_center.x - axes[2].x).abs());
    let ellipsoid_ry = (ellipsoid_center.y - axes[1].y)
        .abs()
        .max((ellipsoid_center.y - axes[2].y).abs());
    let ellipsoid_nrx = (ellipsoid_center.x - axes[3].x)
        .abs()
        .max((ellipsoid_center.x - axes[4].x).abs());
    let ellipsoid_nry = (ellipsoid_center.y - axes[3].y)
        .abs()
        .max((ellipsoid_center.y - axes[4].y).abs());

    //ui.painter().circle_filled(egui::pos2(ellipsoid_center.x, ellipsoid_center.y) + offset, ellipsoid_rx, egui::Color32::GREEN);
    //ui.painter().circle_filled(egui::pos2(ellipsoid_center.x, ellipsoid_center.y) + offset, ellipsoid_ry, egui::Color32::BLUE);
    /*axes.iter()
    .zip([
        egui::Color32::RED,
        egui::Color32::GREEN,
        egui::Color32::BLUE,
    ])
    .for_each(|(a, c)| {
        ui.painter()
            .circle_filled(egui::pos2(a.x, a.y) + offset, 2.0, c);
    });*/
    paint_elliposoid(
        ui,
        egui::pos2(ellipsoid_center.x, ellipsoid_center.y) + offset,
        egui::vec2(ellipsoid_rx, ellipsoid_ry),
        egui::vec2(ellipsoid_nrx, ellipsoid_nry),
        color,
    );
}

pub fn paint_gizmos(
    ui: &mut Ui,
    camera: &Camera,
    pos: Vector3<f32>,
    offset: egui::Vec2,
    res: [u32; 2],
    turning: bool,
) -> Option<Vector3<f32>> {
    let center = camera_transform(camera, pos, res);
    let pointer = ui.input().pointer.hover_pos().unwrap_or(Default::default());
    let world_axes: [Vector3<f32>; 3] = [Vector3::unit_x(), Vector3::unit_y(), Vector3::unit_z()];
    let dirs = [
        (
            camera_transform(camera, pos + Vector3::unit_x(), res),
            egui::color::Color32::RED,
        ),
        (
            camera_transform(camera, pos + Vector3::unit_y(), res),
            egui::color::Color32::GREEN,
        ),
        (
            camera_transform(camera, pos + Vector3::unit_z(), res),
            egui::color::Color32::BLUE,
        ),
    ];

    let center = egui::pos2(center.x, center.y);
    let mut drag_delta = vec3(0.0, 0.0, 0.0);
    let mut one_overed = false;
    for (i, ((p2, color), world_axis)) in dirs.iter().zip(world_axes).enumerate() {
        let screen_center = center + offset;
        let screen_dir = egui::vec2(p2.x, p2.y) - center.to_vec2();
        let screen_p2 = egui::vec2(p2.x, p2.y) + offset;
        let cursor_distance =
            line_point_distance2([screen_center.to_vec2(), screen_p2], pointer.to_vec2());
        let dirdot = screen_dir.dot(pointer - screen_center).signum().max(0.0);
        let is_hovered = dirdot > 0.0
            && cursor_distance < 0.3 * screen_dir.length()
            && (screen_center - pointer).length() < screen_dir.length();
        let gizmpid = egui::Id::new(format!("gizmomemory{i}"));
        if is_hovered && ui.input().pointer.primary_clicked() {
            ui.memory().data.insert_temp(gizmpid, true);
        }
        if !ui.input().pointer.primary_down() {
            ui.memory().data.insert_temp(gizmpid, false);
        }
        let moving = ui.memory().data.get_temp::<bool>(gizmpid).unwrap_or(false);
        let color = if is_hovered || moving {
            egui::color::Color32::YELLOW
        } else {
            *color
        };
        if turning {
            draw_circle(ui, camera, pos, offset, res, Axis::from_i(i as _), color);
        }
        if moving {
            let screen_delta = ui.input().pointer.delta();
            let screen_delta = if turning {
                egui::vec2(screen_delta.y, screen_delta.x)
            } else {
                screen_delta
            };
            let axis_delta = screen_dir.normalized().dot(screen_delta);
            let axis_delta = axis_delta / screen_dir.length();
            drag_delta += axis_delta * world_axis;
            one_overed |= true;
        }
        ui.painter()
            .arrow(screen_center, screen_dir, Stroke::new(1.0, color));
    }

    if one_overed {
        Some(drag_delta)
    } else {
        None
    }
}
