pub mod flycam;
pub mod scripting;
pub mod ui;
pub mod utils;

use std::{
    cell::{RefCell, RefMut},
    rc::Rc,
    time::{Duration, Instant},
};

use game_kernel::{
    ecs::{SystemManager, World},
    subsystems::{video::ui::egui::GkEgui, Subsystems},
    CameraComponent,
};
use scripting::CodeUi;
use ui::EditorUi;

pub struct App {
    pub code_ui: CodeUi,
    pub editor_ui: EditorUi,
}

impl App {
    fn setup_editor_ui(sbsts: &mut Subsystems) -> EditorUi {
        let res = {
            let s = sbsts.window.inner_size();
            [s.width, s.height]
        };

        let mut editor_ui = ui::EditorUi::new(res);

        let offscreen_tetxure_id = sbsts
            .gk_egui
            .painter
            .insert_vulkan_image(sbsts.offscreen_output.image.clone())
            .unwrap();
        editor_ui.state.viewport_tid = offscreen_tetxure_id;

        editor_ui
    }

    pub fn setup_ui(sbsts: &mut Subsystems) -> App {
        Self {
            editor_ui: Self::setup_editor_ui(sbsts),
            code_ui: CodeUi::new(),
        }
    }

    fn editor_ui_frame(
        &mut self,
        gkegui: &mut GkEgui,
        manager: &SystemManager,
        world: Rc<RefCell<World>>,
        should_run: &mut bool,
        full: bool,
    ) {
        let editor_ui = &mut self.editor_ui;
        editor_ui
            .state
            .dbg_ui(gkegui, world.borrow_mut(), should_run);
        editor_ui.draw_tabs(&gkegui.context, &mut *world.borrow_mut());

        manager
            .get_system_mut::<game_kernel::RendererSystem>()
            .unwrap()
            .get_renderer()
            .borrow_mut()
            .set_viewport_size(if full {
                editor_ui.state.viewport_size
            } else {
                [10000, 10000]
            });

        if let (Some(selection), None) = (
            editor_ui.state.viewport_select.take(),
            editor_ui.state.pos_delta,
        ) {
            let mesh_id = manager
                .get_system_mut::<game_kernel::RendererSystem>()
                .unwrap()
                .get_renderer()
                .borrow()
                .fetch_mesh_id(selection);

            let entity_id = world
                .borrow()
                .query::<(
                    game_kernel::utils::KeyType,
                    &game_kernel::StaticMeshComponent,
                )>()
                .find(|(_, mesh)| {
                    mesh.mesh_index()
                        .map(|i| i as u32 == mesh_id)
                        .unwrap_or(false)
                })
                .map(|(mesh_index, _)| mesh_index);
            if let Some(id) = entity_id {
                editor_ui.state.selected_entity = Some(id);
            }
        }

        {
            let camera_entity = manager
                .get_system_mut::<game_kernel::CameraSystem>()
                .unwrap()
                .get_active_camera(&*world.borrow());

            if let Some(camera_entity) = camera_entity {
                editor_ui.state.camera = world
                    .borrow()
                    .get_entity_component::<CameraComponent>(camera_entity)
                    .unwrap()
                    .camera;
            }
        }
    }

    pub fn ui_frame(&mut self, sbsts: &mut Subsystems, should_run: &mut bool, full: bool) {
        self.editor_ui_frame(
            &mut sbsts.gk_egui,
            &sbsts.systems_manager,
            sbsts.world.clone(),
            should_run,
            full,
        );
        self.code_ui.script_ui(
            sbsts.wren_scripting_engine.clone(),
            &sbsts.gk_egui.context,
            sbsts.world.clone(),
        );
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        let result = 2 + 2;
        assert_eq!(result, 4);
    }
}
