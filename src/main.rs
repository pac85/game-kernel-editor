use std::{
    cell::{RefCell, RefMut},
    rc::Rc,
    time::{Duration, Instant},
};

use game_kernel::{
    ecs::{SystemManager, World},
    subsystems::{video::ui::egui::GkEgui, Subsystems},
    CameraComponent,
};
use game_kernel_editor::{scripting::CodeUi, ui::EditorUi, *};

fn main() {
    let mut sbsts = game_kernel::subsystems::init().unwrap();
    game_kernel::core_systems::init_systems(&mut sbsts);

    let mut flycam_s = flycam::Flycam::setup_flycam(&mut sbsts);

    sbsts.physfs().mount("wad.pac", "/", false).unwrap();
    sbsts.load("untitled.gkw.json");

    let mut app = App::setup_ui(&mut sbsts);

    let mut should_run = true;
    let mut prev_time = Instant::now();
    while should_run
        && sbsts.process_frame(
            |delta, _elapsed, sbsts| {
                let mut input = sbsts
                    .systems_manager
                    .get_resource_mut::<game_kernel::input::Input>();
                flycam_s.flycam(&sbsts.world.borrow_mut(), delta, &mut *input);

                drop(input);
                app.ui_frame(sbsts, &mut should_run, true);
                if app.editor_ui.state.viewport_pointer_pos.is_some()
                    && app.editor_ui.state.pos_delta.is_none()
                {
                    let pdelta = sbsts.gk_egui.context.input().pointer.delta();
                    flycam_s.mouse_pos(cgmath::vec2(pdelta.x, pdelta.y));
                }

                std::thread::sleep(Duration::from_millis(14));
            },
            false,
            &mut prev_time,
        )
    {}

    /*sbsts.main_loop(
        move |delta,
              timestamp,
              mouse_pos,
              buttons,
              world,
              ui,
              user_events,
              manager,
              gkegui,
              captured| {


            let input = manager.get_resource::<game_kernel::input::Input>();
            flycam_s.mouse_pos(mouse_pos);
            flycam_s.flycam(&world.borrow_mut(), delta, &*input);

            ui_frame(&mut editor_ui, gkegui, manager, world, &mut should_run, !*captured);

            std::thread::sleep(Duration::from_millis(14));

            should_run
        },
    );*/
}
