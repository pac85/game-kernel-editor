use std::fs::File;
use std::io::{Read, Seek, Write};
use std::{cell::RefCell, rc::Rc};

use ::game_kernel::{
    subsystems::Subsystems,
    wren_scripting::{WrenScriptingEngine, WrenWorld},
};
use game_kernel::ecs::{World, META_STRUCT_FACTORY};
use game_kernel::ruwren;

pub mod syntax_highlighting;

const TEST_SCRIPT: &str = r##"
System.print("hello from wren")

import "math" for Vec2, Vec3, Vec4, Mat2
var tvec3 = Vec3.new(1, 2, 3)
System.print(tvec3.toString())

main = Fn.new {|world|
    var entities = world.getEntities()
    for(entity in entities) {
        System.print("entity")
        System.print(entity)
        var components = world.getEntityComponents(entity)
        for(component in components) {
            System.print(component.toString())
            if(component.toString() == "TagComponent" && component.getField("name") == "Player entity") {
                System.print(component.getField("name"))
                System.print("////////////Found player")
            }
            var position = component.getField("position")
            if (position) {
                System.print("Position:" + component.getField("position").toString())
            }
        }
    }
    //System.print(entities)
}
"##;

pub fn test(sbsts: &Subsystems) {
    return;
    sbsts
        .wren_scripting_engine
        .borrow()
        .vm
        .interpret("main", TEST_SCRIPT)
        .unwrap();

    sbsts.wren_scripting_engine.borrow().vm.execute(|vm| {
        vm.ensure_slots(1);
        vm.set_slot_new_foreign("ecs", "World", WrenWorld::new(sbsts.world.clone()), 1)
            .unwrap();
        vm.get_variable("main", "main", 0);
        sbsts
            .wren_scripting_engine
            .borrow()
            .vm
            .call(ruwren::FunctionSignature::new_function("call", 1))
            .unwrap();
    });
}

pub fn create_component_classes(vm: Rc<RefCell<WrenScriptingEngine>>) {
    let mut source = String::new();
    source += r##"
class ComponentWrapper {
    construct new(inner) {_inner = inner}
    typeName() {_inner.typeName()}
    getField(name) {_inner.getField(name)}
    setField(name, value) {_inner.setField(name, value)}

    toString() {_inner.toString()}
}

class WorldWrapper {
    construct new(inner) {
        _inner = inner
    }
    getEntities() { _inner.getEntities()}
    getEntityComponents(entity) {
        return _inner.getEntityComponents(entity).map {|component|
            return WorldWrapper.wrapComponent(component)
        }
    }
    static newClassMap() {__componentClassesMap = {}}
    static addComponentClass(name, c) {__componentClassesMap[name] = c}
    static getComponentClass(name) {__componentClassesMap[name]}
    static wrapComponent(component) {
        var cl = WorldWrapper.getComponentClass(component.typeName())
        return cl ? cl.new(component) : ComponentWrapper.new(component)
    }
}
WorldWrapper.newClassMap()
    "##;

    let components = META_STRUCT_FACTORY.lock().unwrap();
    components.all_names().for_each(|component_name| {
        source += &format!("class {component_name} is ComponentWrapper {{\n");
        source += "    construct new(inner) {super(inner)}\n";
        let fields = components.get_fields(&component_name);
        for (name, field) in fields.unwrap() {
            source += &format!("    {name} {{ super.getField(\"{name}\") }} \n");
            source += &format!("    {name}=(value) {{ super.setField(\"{name}\", value) }} \n");
        }
        source += "}\n";
        source +=
            &format!("WorldWrapper.addComponentClass(\"{component_name}\", {component_name})\n");
    });
    let r = vm.borrow().vm.interpret("ecs", &source);
    println!("{source}");
    r.unwrap();
}

pub struct CodeUi {
    code: String,
    opened_file: Option<String>,
    errors: Option<String>,
    run_enabled: bool,
}

impl CodeUi {
    pub fn new() -> Self {
        Self {
            code: "".to_owned(), //TEST_SCRIPT.to_string(),
            opened_file: None,
            errors: Some("".to_string()),
            run_enabled: true,
        }
    }

    pub fn run_script(
        &mut self,
        vm: Rc<RefCell<WrenScriptingEngine>>,
        ctx: &egui::Context,
        world: Rc<RefCell<World>>,
    ) {
        let mut main_declared = false;
        vm.borrow().vm.execute(|vm| {
            vm.ensure_slots(2);
            vm.set_slot_new_foreign("ecs", "World", WrenWorld::new(world), 1);
            vm.get_variable("ecs", "WorldWrapper", 0);
        });
        vm.borrow()
            .vm
            .call(ruwren::FunctionSignature::new_function("new", 1))
            .unwrap();
        {
            let vm = vm.borrow();
            let wrapper_slot_handle = vm.vm.get_slot_handle(0);
            vm.vm.set_slot_handle(1, &wrapper_slot_handle);
        }
        vm.borrow().vm.execute(|vm| {
            main_declared = vm.get_variable("test", "main", 0);
        });

        let r = vm
            .borrow()
            .vm
            .call(ruwren::FunctionSignature::new_function("call", 1));

        println!("{r:?}");

        if !main_declared {
            vm.borrow().vm.interpret("test", "var main = Fn.new {}");
        }
    }

    pub fn script_ui(
        &mut self,
        vm: Rc<RefCell<WrenScriptingEngine>>,
        ctx: &egui::Context,
        world: Rc<RefCell<World>>,
    ) {
        egui::Window::new("Script").show(ctx, |ui| {
            ui.horizontal(|ui| {
                if ui.button("▶").clicked() {
                    let run_code = self.code.clone();

                    let source = format!(
                        "
                        (Fn.new {{
                            {}
                        }}).call()
                        ",
                        run_code,
                    );
                    let r = vm.borrow().vm.interpret("test", &source);

                    if let Err(ref e) = r {
                        self.errors = Some(format!("{e}"));
                    } else {
                        self.errors = None;
                    }
                }

                if ui.button("open").clicked() {
                    self.opened_file = Some("".to_owned());
                }
                if let Some(opened_file) = self.opened_file.as_mut() {
                    ui.text_edit_singleline(opened_file);
                }
                if ui.button("💾").clicked() && self.opened_file.is_some() {
                    let mut file = File::create(self.opened_file.as_ref().unwrap()).unwrap();
                    file.write_all(self.code.as_bytes()).unwrap();
                    file.flush();
                }
                if ui.button("load").clicked() && self.opened_file.is_some() {
                    let mut file = File::open(self.opened_file.as_ref().unwrap()).unwrap();
                    file.seek(std::io::SeekFrom::End(0)).unwrap();
                    let len = file.stream_position().unwrap();
                    let mut data = Vec::new();
                    data.resize(len as usize, 0);
                    file.seek(std::io::SeekFrom::Start(0)).unwrap();
                    file.read(data.as_mut_slice()).unwrap();
                    self.code = String::from_utf8(data).unwrap();
                }
            });

            let mut theme = syntax_highlighting::CodeTheme::from_memory(ui.ctx());
            ui.collapsing("Theme", |ui| {
                ui.group(|ui| {
                    theme.ui(ui);
                    theme.clone().store_in_memory(ui.ctx());
                });
            });

            let mut layouter = |ui: &egui::Ui, string: &str, wrap_width: f32| {
                let mut layout_job =
                    syntax_highlighting::highlight(ui.ctx(), &theme, string, "wren".into());
                //layout_job.wrap.max_width = wrap_width;
                ui.fonts().layout_job(layout_job)
            };

            ui.separator();
            egui::ScrollArea::vertical()
                .max_height(500.0)
                .show(ui, |ui| {
                    ui.add(
                        egui::TextEdit::multiline(&mut self.code)
                            .font(egui::TextStyle::Monospace) // for cursor height
                            .code_editor()
                            .desired_rows(10)
                            .lock_focus(true)
                            .desired_width(f32::INFINITY)
                            .layouter(&mut layouter),
                    );
                });
            ui.separator();

            let mut err = format!("{:?}", self.errors);
            egui::TextEdit::multiline(&mut err).show(ui);
            ui.checkbox(&mut self.run_enabled, "run");
            if !self.run_enabled {
                return;
            }
        });
    }
}
